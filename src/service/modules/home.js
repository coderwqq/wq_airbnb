import wqRequest from "../request";

export function getHomeGoodPriceData() {
  return wqRequest.get({
    url: "/home/goodprice"
  })
}

export function getHomeHighScoreData() {
  return wqRequest.get({
    url: "/home/highscore"
  })
}

export function getHomediscountData() {
  return wqRequest.get({
    url: "/home/discount"
  })
}

export function getHotrecommendData() {
  return wqRequest.get({
    url: "/home/hotrecommenddest"
  })
}

export function getLongforData() {
  return wqRequest.get({
    url: "/home/longfor"
  })
}

export function getPlusData() {
  return wqRequest.get({
    url: "/home/plus"
  })
}