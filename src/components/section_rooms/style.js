import styled from "styled-components";

const SectionRoomsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -10px;
`
export default SectionRoomsWrapper