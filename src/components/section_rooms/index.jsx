import PropTypes from 'prop-types'
import React, { memo } from 'react'
import RoomItem from '../room_item'
import SectionRoomWrapper from './style'

const SectionRooms = memo((props) => {
  const { roomList, itemWidth } = props

  return (
    <SectionRoomWrapper>
      {
        roomList?.slice(0, 8).map(item => {
          return (
            <RoomItem itemData={item} key={item.id} itemWidth={itemWidth}/>
          )
        })
      }
    </SectionRoomWrapper>
  )
})

SectionRooms.propTypes = {
  roomList: PropTypes.array
}

export default SectionRooms