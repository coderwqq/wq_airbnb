import styled from "styled-components";

const LongforItemWrapper = styled.div`
  width: 20%;
  flex-shrink: 0;

  
  .info {
    position: relative;
    padding: 0 8px;

    .text {
      position: absolute;
      color: #fff;
      text-align: center;
      left: 0;
      right: 0;
      bottom: 30px;
      margin: 0 auto;

      .city {
        font-size: 18px;
        font-weight: 700;
      }
      .price {
        font-size: 14px;
        margin-top: 5px;
      }
    }
    .background {
      position: relative;
      border-radius: 3px;
      overflow: hidden;

      .cover {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        height: 50%;
        background-image: linear-gradient(-180deg, rgba(0,0,0,0) 3%, rgb(0,0,0) 100%);
      }
      img {
        width: 100%;
      }
    }
  }
`
export default LongforItemWrapper