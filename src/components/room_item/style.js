import styled from "styled-components";

const RoomItemWrapper = styled.div`
  flex-shrink: 0;
  box-sizing: border-box;
  width: ${props => props.itemWidth};
  padding: 10px;

  .inner {
    .swiper {
      position: relative;
      color: #fff;

      &:hover {
        .arrow {
          display: flex;
        }
      }
      .arrow {
        position: absolute;
        display: none;
        justify-content: center;
        align-items: center;
        width: 70px;
        height: 100%;
        cursor: pointer;

        &.left {
          left: 0;
          top: 50%;
          transform: translate(0, -50%);
          background: linear-gradient(to left, transparent 1%, rgba(0,0,0,.05) 100%);
          
        }
        &.right {
          right: 0;
          top: 50%;
          transform: translate(0, -50%);
          background: linear-gradient(to right, transparent 1%, rgba(0,0,0,.05) 100%);
        }
      }
      .indicator {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 10px;
        margin: 0 auto;
        width: 30%;

        .item-dots {
          display: flex;
          justify-content: center;
          align-items: center;
          width: 20%;

          .dot {
            width: 6px;
            height: 6px;
            border-radius: 50%;
            background-color: rgba(255,255,255,.9);
            cursor: pointer;

            &.active {
              width: 8px;
              height: 8px;
              border-radius: 50%;
            }
          }
        }
      }
    }
    .cover {
      position: relative;
      padding-top: 66.66%;
      border-radius: 5px;
      overflow: hidden;
      cursor: pointer;

      img {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
    }
    .verify {
      color: ${props => props.verifyColor};
      font-size: 12px;
      font-weight: 700;
      padding: 10px 0 5px;
    }
    .name {
      font-size: 16px;
      font-weight: 700;

      text-overflow: ellipsis;
      overflow: hidden;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
    }
    .price {
      font-size: 14px;
      padding: 8px 0;
    }
    .buttom {
      display: flex;
      align-items: center;

      .MuiRating-decimal {
        margin-right: -2px;
      }
      .count {
        font-size: 12px;
        font-weight: 700;
        padding: 0 2px 0 4px;
      }
      .content {
        font-size: 12px;
        font-weight: 700;
      }
    }
  }
`
export default RoomItemWrapper