import PropTypes from 'prop-types'
import React, { memo, useRef, useState } from 'react'
import { Rating } from '@mui/material'
import { Carousel } from 'antd'

import RoomItemWrapper from './style'
import IconArrowLeft from '@/assets/svg/icon_arrow_left'
import IconArrowRight from '@/assets/svg/icon_arrow_right'
import Indicator from '@/base_ui/indicator'
import classNames from 'classnames'

const RoomItem = memo((props) => {
  const { itemData = [], itemWidth = "25%", roomClick } = props

  const [selectIndex, setSelectIndex] = useState(0)
  const carouselRef = useRef()
  const pictureRef = useRef()

  function arrowClickHandle(isRight) {
    isRight ? carouselRef.current.next() : carouselRef.current.prev()
    let newIndex = isRight ? selectIndex + 1 : selectIndex - 1

    const length = itemData.picture_urls.length
    if(newIndex < 0) newIndex = length - 1
    if(newIndex > length - 1) newIndex = 0
    setSelectIndex(newIndex)
  }

  function roomClickHandle() {
    if(roomClick) roomClick(itemData)
  }

  return (
    <RoomItemWrapper 
      verifyColor={itemData?.verify_info?.text_color}
      itemWidth={itemWidth}
    >
      <div className='inner'>
        {
          itemData.picture_urls ? (
            <div className='swiper'>
              <div className='carousel' onClick={roomClickHandle}>
                <Carousel dots={false} ref={carouselRef}>
                  {
                    itemData?.picture_urls?.map(item => {
                      return (
                        <div className='cover' key={item} ref={pictureRef}>
                          <img src={item} alt="" />
                        </div>
                      )
                    })
                  }
                </Carousel>
              </div>
              <div className='indicator'>
                <Indicator selectIndex={selectIndex}>
                  {
                    itemData.picture_urls.map((item, index) => {
                      return (
                        <div 
                          className='item-dots' 
                          key={item}
                        >
                          <span 
                            className={classNames('dot', {active: selectIndex === index})}
                          >
                          </span>
                        </div>
                      )
                    })
                  }
                </Indicator>
              </div>
              <div className='arrow left' onClick={() => arrowClickHandle(false)}>
                <IconArrowLeft width="25" height="25"/>
              </div>
              <div className='arrow right' onClick={() => arrowClickHandle(true)}>
                <IconArrowRight width="25" height="25"/>
              </div>
            </div>
          ) : 
          <div className='cover'>
            <img src={itemData.picture_url} alt="" />
          </div>
        }
        <div className='verify'>{itemData.verify_info.messages.join("·")}</div>
        <div className='name'>{itemData.name}</div>
        <div className='price'>¥{itemData.price}/晚</div>
        <div className='buttom'>
          <Rating
            readOnly
            value={itemData.star_rating ?? 3}
            precision={0.1}
            sx={{fontSize: "12px", color: itemData.star_rating_color}}
          />
          <span className='count'>{itemData.reviews_count}</span>
          {
            itemData?.bottom_info && <span className='content'>·{itemData?.bottom_info?.content}</span>
          }
        </div>
      </div>
    </RoomItemWrapper>
  )
})

RoomItem.propTypes = {
  itemData: PropTypes.object
}

export default RoomItem