import styled from "styled-components";

const SectionHeaderWrapper = styled.div`
  .title {
    font-size: 22px;
    margin-bottom: 16px;
    color: #222;
  }
  .subtitle {
    font-size: 16px;
    margin-bottom: 20px
  }
`

export default SectionHeaderWrapper