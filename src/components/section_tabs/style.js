import styled from "styled-components";

const TabsWrapper = styled.div`
  padding: 8px 0;

  .item {
    box-sizing: border-box;
    flex-basis: 120px;
    flex-shrink: 0;
    text-align: center;
    white-space: nowrap;
    font-size: 17px;
    padding: 14px 16px;
    margin-right: 16px;
    border: 1px solid ${props => props.theme.color.lineColor};
    border-radius: 3px;
    cursor: pointer;
    ${props => props.theme.mixin.boxShadow}

    &.active {
      color: #fff;
      background-color: ${props => props.theme.color.secondaryColor};
    }
  }
`
export default TabsWrapper