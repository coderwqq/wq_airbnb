import PropTypes from 'prop-types'
import React, { memo, useState } from 'react'
import classNames from 'classnames'

import TabsWrapper from './style'
import ScrollView from '@/base_ui/scroll_view'

const SectionTabs = memo((props) => {
  const { tabsName, tabsClick } = props

  const [ currentIndex, setCurrentIndex ] = useState(0)

  function itemClickHandle(index, item) {
    setCurrentIndex(index)
    tabsClick(item)
  }

  return (
    <TabsWrapper>
      <ScrollView>
        {
          tabsName?.map((item, index) => {
            return (
              <div 
                key={item} 
                className={classNames("item", {active: index === currentIndex})}
                onClick={() => itemClickHandle(index, item)}
              >
                {item}
              </div>
            )
          })
        }
      </ScrollView>
    </TabsWrapper>
  )
})

SectionTabs.propTypes = {
  tabsName: PropTypes.array
}

export default SectionTabs