import styled from "styled-components";

const FooterWrapper = styled.div`
  display: flex;
  margin-top: 10px;

  .info {
    display: flex;
    align-items: center;
    cursor: pointer;
    color: ${props => props.color};

    .text {
      font-size: 17px;
      font-weight: 700;
      margin-right: 6px;

      &:hover {
        text-decoration: underline;
      }
    }
  }
`
export default FooterWrapper