import IconArrow from '@/assets/svg/icon_arrow'
import PropTypes from 'prop-types'
import React, { memo } from 'react'
import { useNavigate } from 'react-router-dom'
import FooterWrapper from './style'

const SectionFooter = memo((props) => {
  const { name } = props

  let showMessage = "显示全部"
  if(name) {
    showMessage = `显示更多${name}房源`
  }

  const navigate = useNavigate()
  function showMoreClick() {
    navigate("/entire")
  }

  return (
    <FooterWrapper color={name ? "#00848A" : "#484848"}>
      <div className='info' onClick={showMoreClick}>
        <span className='text'>{showMessage}</span>
        <IconArrow/>
      </div>
    </FooterWrapper>
  )
})

SectionFooter.propTypes = {
  name: PropTypes.string
}

export default SectionFooter