import styled from "styled-components";

export const RightWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: flex-end;
  cursor: pointer;
  color: ${props => props.theme.text.secondaryColor};

  .btns {
    display: flex;
    align-items: center;
    color: ${props => props.theme.isAlpha ? "#fff" : props.theme.text.primaryColor};
    
    .btn {
      padding: 12px 15px;
      font-weight: 700;

      &:hover {
        background-color: ${props => props.theme.isAlpha ? "rgba(255,255,255,.1)" : "#f5f5f5"};
        border-radius: 20px;
      }
    }
  }
  .profile {
    position: relative;
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 77px;
    height: 42px;
    box-sizing: border-box;
    padding: 5px 5px 5px 12px;
    border: 1px solid #ddd;
    border-radius: 21px;
    background-color: ${props => props.theme.isAlpha ? "#fff" : ""};

    ${props => props.theme.mixin.boxShadow}

    .panel {
     position: absolute;
      width: 240px;
      top: 52px;
      right: 0;
      background-color: #fff;
      box-shadow: 0 0 6px rgba(0,0,0,.2);
      border-radius: 5px;

      .top-item, .bottom-item {
        padding: 10px 0;
      }
      .top-item {
        border-bottom: 1px solid ${props => props.theme.color.lineColor};
      }
      .item {
        font-weight: 700;
        height: 40px;
        line-height: 40px;
        padding: 0 16px;
        cursor: pointer;
        &:hover {
          background-color: #eee;
        }
      }
    }
  }

  
`