import styled from "styled-components";

export const CenterWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  height: 48px;

  .search-bar {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 8px;
    width: 300px;
    height: 48px;
    box-sizing: border-box;
    border: 1px solid ${props => props.theme.color.lineColor};
    border-radius: 24px;

    .text {
      padding: 0 16px;
      font-weight: 700;
    }
    .icon {
      display: flex;
      justify-content: center;
      align-items: center;
      width: 32px;
      height: 32px;
      color: #fff;
      background-color: ${props => props.theme.color.primaryColor};
      border-radius: 16px;
      cursor: pointer;
    }
    ${props => props.theme.mixin.boxShadow}
  }

  .detail-enter {
    transform: scale(0.35, 0.7272) translateY(-58px);
    opacity: 0;
  }
  .detail-enter-active {
    transform: scale(1.0) translateY(0);
    opacity: 1;
    transition: all 250ms ease;
  }
  .detail-exit {
    transform: scale(1.0) translateY(0);
    opacity: 1;
  }
  .detail-exit-active {
    transform: scale(0.35, 0.7272) translateY(-58px);
    opacity: 0;
    transition: all 250ms ease;
  }

  .bar-enter {
    transform: scale(2.8571, 1.375) translateY(58px);
    opacity: 0;
  }
  .bar-enter-active {
    transform: scale(1.0) translateY(0);
    opacity: 1;
    transition: all 250ms ease;
  }
  .bar-exit {
    opacity: 0;
  }
`