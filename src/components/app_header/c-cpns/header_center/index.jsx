import React, { memo, useState } from 'react'
import { CSSTransition } from 'react-transition-group'

import { CenterWrapper } from './style'
import IconSearch from '@/assets/svg/icon_search'
import SearchSection from './c-cpns/search_section'
import SearchTabs from './c-cpns/search_tabs'
import searchInfos from "@/assets/data/search_titles"

const HeaderCenter = memo((props) => {
  const { showSearch, searchBarClick } = props

  const titles = searchInfos.map(item => item.title)
  const infos = searchInfos.map(item => item.searchInfos)
  const [recordIndex, setRecordIndex] = useState(0)

  function titleClickHandle(index) {
    setRecordIndex(index) 
  }
  function searchClickHandle() {
    if(searchBarClick) searchBarClick()
  }

  return (
    <CenterWrapper>
      <CSSTransition
        in={!showSearch}
        classNames="bar"
        timeout={250}
        unmountOnExit={true}
      >
        <div className='search-bar' onClick={searchClickHandle}>
          <div className='text'>搜索房源和体验</div>
          <div className='icon'>
            <IconSearch/>
          </div>
        </div>
      </CSSTransition>
      <CSSTransition
        in={showSearch}
        classNames="detail"
        timeout={250}
        unmountOnExit={true}
      >
        <div className='search-detail'>
          <SearchTabs searchTitles={titles} titleClick={titleClickHandle}/>
          <SearchSection searchInfos={infos} recordIndex={recordIndex}/>
        </div>
      </CSSTransition> 
    </CenterWrapper>
  )
})

export default HeaderCenter