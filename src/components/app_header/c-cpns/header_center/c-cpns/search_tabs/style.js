import styled from "styled-components";

export const TabsWrapper = styled.div`
  display: flex;
  font-size: 16px;
  color: ${props => props.theme.isAlpha ? "#fff" : "#222"};

  .item {
    .title {
      padding: 15px 15px 8px;
      cursor: pointer;
    }
    .underline {
      margin: 0 15px;
      border-bottom: 2px solid ${props => props.theme.isAlpha ? "#fff" : "#222"};
    }
  }
`