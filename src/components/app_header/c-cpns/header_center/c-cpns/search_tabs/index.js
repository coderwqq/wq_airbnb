import PropTypes from 'prop-types'
import React, { memo, useState } from 'react'

import { TabsWrapper } from "./style"

const SearchTabs = memo((props) => {
  const { searchTitles, titleClick } = props
  const [selectIndex, setSelectIndex] = useState(0)

  function tabClickHandle(index) {
    if(titleClick) titleClick(index)
    setSelectIndex(index)
  }

  return (
    <TabsWrapper>
      {
        searchTitles.map((item, index) => {
          return (
            <div 
              className='item' 
              key={item}
              onClick={() => tabClickHandle(index)}
            >
              <div className='title'>{item}</div>
              { selectIndex === index && <div className='underline'></div> }
            </div> 
          )
        })
      }
    </TabsWrapper>
  )
})

SearchTabs.propTypes = {
  searchTitles: PropTypes.array
}

export default SearchTabs