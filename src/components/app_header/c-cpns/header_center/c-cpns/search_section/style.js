import styled from "styled-components";

export const SectionWrapper = styled.div`
  position: absolute;
  left: -326px;
  top: 80px;
  display: flex;
  justify-content: center;
  width: 840px;
  height: 66px;
  border: 1px solid ${props => props.theme.color.lineColor};
  border-radius: 33px;
  background-color: ${props => props.theme.isAlpha ? "#fff" : ""};

  .wrapper-search {
    display: flex;
    justify-content: center;

    .inner {
      display: flex;
      padding: 14px 0;
      box-sizing: border-box;
      cursor: pointer;

      &:hover {
        background-color: #f3f3f3;
        border-radius: 33px;
        .line {
          visibility: hidden;
        }
      }
      .search-text {
        width: ${props => props.recordIndex === 0 ? 215 : 356}px;
        padding: 0 32px;

        .title {
          color: #222;
          font-weight: 700;
          margin-bottom: 4px;
        }
      }
      .line {
        width: 1px;
        height: 38px;
        background-color: ${props => props.theme.color.lineColor};
      }
    }
  }
  .search-icon {
    position: absolute;
    top: 9px;
    right: 9px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 48px;
    height: 48px;
    color: #fff;
    border-radius: 50%;
    background-color: ${props => props.theme.color.primaryColor};
  }
`