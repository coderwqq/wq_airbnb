import IconSearch from '@/assets/svg/icon_search'
import PropTypes from 'prop-types'
import React, { memo } from 'react'

import { SectionWrapper } from './style'

const SearchSection = memo((props) => {
  const { searchInfos, recordIndex } = props

  const selectIndex = searchInfos[recordIndex].length - 1

  return (
    <SectionWrapper recordIndex={recordIndex}>
      <div className='wrapper-search'>
        {
          searchInfos[recordIndex].map((item, index) => {
            return (
              <div className='inner' key={index}>
                <div className='search-text'>
                  <div className='title'>{item.title}</div>
                  <div className='desc'>{item.desc}</div>
                </div>
                {
                  !(selectIndex === index) ? <div className='line'></div> : ""
                }
              </div>
            )
          })
        }
      </div>
      <div className='search-icon'>
        <IconSearch width="16" height="16"/>
      </div>
    </SectionWrapper>
  )
})

SearchSection.propTypes = {
  searchInfos: PropTypes.array
}

export default SearchSection