import React, { memo } from 'react'
import { useNavigate } from 'react-router-dom'

import { LeftWrapper } from './style'
import IconLogo from '@/assets/svg/icon_logo'

const HeaderLeft = memo(() => {
  const navigate = useNavigate()
  function logoClick() {
    navigate("/home")
  }

  return (
    <LeftWrapper>
      <div className='iconLogo' onClick={logoClick}>
        <IconLogo/>
      </div>
    </LeftWrapper>
  )
})

export default HeaderLeft