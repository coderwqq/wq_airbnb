import React, { memo, useRef, useState } from 'react'
import classNames from 'classnames'
import { shallowEqual, useSelector } from 'react-redux'

import { useScrollPosition } from '@/utils/useScrollPosition'
import { HeaderWrapper, SearchAreaWrapper } from './style'
import HeaderCenter from './c-cpns/header_center'
import HeaderLeft from './c-cpns/header_left'
import HeaderRight from './c-cpns/header_right'
import { ThemeProvider } from 'styled-components'


const AppHeader = memo(() => {
  const [showSearch, setShowSearch] = useState(false)

  const { headerConfig } = useSelector((state) => ({
    headerConfig: state.mainReducer.headerConfig
  }), shallowEqual)
  const { isFixed, padding, topAlpha } = headerConfig

  // 监听位置的滚动
  const prevY = useRef(0)
  const { scrollY } = useScrollPosition()
  if(!showSearch) prevY.current = scrollY
  if(showSearch && Math.abs(scrollY - prevY.current) > 200) setShowSearch(false)

  // 顶部透明逻辑
  const isAlpha = topAlpha && scrollY === 0

  return (
    <ThemeProvider theme={{isAlpha}}>
      <HeaderWrapper 
        className={classNames({isFixed: isFixed})}
        padding={padding}
      >
        <div className='content'>
          <div className='top'>
          <HeaderLeft/>
          <HeaderCenter showSearch={isAlpha || showSearch} searchBarClick={() => setShowSearch(true)}/>
          <HeaderRight/>
          </div>
          <SearchAreaWrapper showSearch={showSearch}/>
        </div>
        { showSearch && <div className='cover' onClick={() => setShowSearch(false)}></div> }
      </HeaderWrapper>
    </ThemeProvider>
  )
})

export default AppHeader