import styled from "styled-components";

export const HeaderWrapper = styled.div`
  &.isFixed {
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    z-index: 99;
  }
  .content {
    position: relative;
    z-index: 9;
    background-color: ${props => props.theme.isAlpha ? "" : "#fff"};
    border-bottom: 1px solid ${props => props.theme.isAlpha ? "rgba(255,255,255,0)" : "#eee"};
    
    .top {
      display: flex;
      align-items: center;
      height: 80px;
      padding: 0 ${props => props.padding}px;
      
    }
  }
  .cover {
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background-color: rgba(0,0,0,.3);
  }
`
export const SearchAreaWrapper = styled.div`
  transition: height 250ms ease;
  height: ${props => props.showSearch ? "100px" : "0"}
`