import styled from "styled-components";

const FooterWrapper = styled.div`
  margin-top: 100px;
  border-top: 1px solid ${props => props.theme.color.lineColor};
  
  .wrapper {
    width: 1032px;
    margin: 0 auto;
    padding: 48px 0;

    .service {
      display: flex;

      .item {
        width: 25%;

        .title {
          color: ${props => props.theme.text.secondaryColor};
          margin-bottom: 16px;
        }
        .iten {
          color: ${props => props.theme.text.thirdColor};
          margin-top: 6px;
        }
      }
    }
    .complain {
      display: flex;
      justify-content: center ;
      padding: 20px;
      margin-top: 30px;
      color: ${props => props.theme.text.thirdColor};
      border-top: 1px solid ${props => props.theme.color.lineColor};
    }
  }
`

export default FooterWrapper