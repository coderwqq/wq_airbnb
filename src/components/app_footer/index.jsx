import React, { memo } from 'react'

import FooterWrapper from './style'
import footerData from "@/assets/data/footer.json"

const AppFooter = memo(() => {
  return (
    <FooterWrapper>
      <div className='wrapper'>
        <div className='service'>
          {
            footerData.map(item => {
              return (
                <div key={item.name} className="item">
                  <h4 className='title'>{item.name}</h4>
                  <ul>
                    {
                      item.list.map(iten => {
                        return (
                          <li key={iten} className="iten">{iten}</li>
                        )
                      })
                    }
                  </ul>
                </div>
              )
            })
          }
        </div>
        <div className='complain'>© 2022 Airbnb, Inc. All rights reserved.条款 · 隐私政策 · 网站地图 · 全国旅游投诉渠道 12301</div>
      </div>
    </FooterWrapper>
  )
})

export default AppFooter