import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { 
  getHomeGoodPriceData, 
  getHomeHighScoreData,
  getHomediscountData,
  getHotrecommendData,
  getLongforData,
  getPlusData
} from "@/service/modules/home";

// export const fetchHomeDataAction = createAsyncThunk("fetchHomeData", 
//   async () => {
//     const res = await getHomeGoodPriceData()
//     return res
//   }
// )
export const fetchHomeDataAction = createAsyncThunk("fetchHomeData", 
  (payload, {dispatch }) => {
    getHomeGoodPriceData().then(res => {
      dispatch(changeGoodPriceDataAction(res))
    })
    getHomeHighScoreData().then(res => {
      dispatch(changeHighScoreDataAction(res))
    })
    getHomediscountData().then(res => {
      dispatch(changeDiscountDataAction(res))
    })
    getHotrecommendData().then(res => {
      dispatch(changeHotrecommendDataAction(res))
    })
    getLongforData().then(res => {
      dispatch(changeLongforDataAction(res))
    })
    getPlusData().then(res => {
      dispatch(changePlusDataAction(res))
    })
  }
)

const homeSlice = createSlice({
  name: "home",
  initialState: {
    goodPriceData: {},
    highScoreData: {},
    discountData: {},
    hotrecommendData: {},
    longforData: {},
    plusData: {}
  },
  reducers: {
    changeGoodPriceDataAction(state, { payload }) {
      state.goodPriceData = payload
    },
    changeHighScoreDataAction(state, { payload }) {
      state.highScoreData = payload
    },
    changeDiscountDataAction(state, { payload }) {
      state.discountData = payload
    },
    changeHotrecommendDataAction(state, { payload }) {
      state.hotrecommendData = payload
    },
    changeLongforDataAction(state, { payload }) {
      state.longforData = payload
    },
    changePlusDataAction(state, { payload }) {
      state.plusData = payload
    }
  },
  // extraReducers: {
  //   [fetchHomeDataAction.fulfilled](state, { payload }) {
  //     state.goodPriceData = payload
  //   }
  // }
})

export const { 
  changeGoodPriceDataAction,
  changeHighScoreDataAction,
  changeDiscountDataAction,
  changeHotrecommendDataAction,
  changeLongforDataAction,
  changePlusDataAction
} = homeSlice.actions

export default homeSlice.reducer