import { CHANGE_CURRENT_PAGE, CHANGE_IS_LODING, CHANGE_ROOM_LIST, CHANGE_TOTAL_COUNT } from "./constant";
import { getEntireRoomlistData } from "@/service/modules/entire";

export const changeCurrentPageAction = (currentPage) => ({
  type: CHANGE_CURRENT_PAGE,
  currentPage
})

export const changeRoomlistAction = (roomList) => ({
  type: CHANGE_ROOM_LIST,
  roomList
})

export const changeTotalCountAction = (totalCount) => ({
  type: CHANGE_TOTAL_COUNT,
  totalCount
})

export const changeIslodingAction = (isLoding) => ({
  type: CHANGE_IS_LODING,
  isLoding
})

export const fetchEntireListDataAction = (page = 0) => {
  return async function(dispatch, getState) {

    dispatch(changeIslodingAction(true))
    // 根据页码获取对应的数据
    const res = await getEntireRoomlistData(page * 20)
    dispatch(changeIslodingAction(false))

    const roomList = res.list
    const totalCount = res.totalCount
    dispatch(changeRoomlistAction(roomList))
    dispatch(changeTotalCountAction(totalCount))
    dispatch(changeCurrentPageAction(page))
  }
}