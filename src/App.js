import React, { memo, useEffect } from 'react'
import { useLocation, useRoutes } from 'react-router-dom'

import routes from './router'
import AppFooter from './components/app_footer'

const App = memo(() => {
  // 当页面发生跳转时，自动跳转到顶部
  const location = useLocation()
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [location.pathname])

  return (
    <div>
      <div className='pages'>
        {useRoutes(routes)}
      </div>
      <AppFooter/>
    </div>
  )
})

export default App
