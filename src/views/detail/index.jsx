import React, { memo, useEffect } from 'react'

import { DetailWrapper } from './style'
import DetailInfos from './c-cpns/detail_infos'
import DetailPicture from './c-cpns/detail_picture'
import AppHeader from '@/components/app_header'
import { useDispatch } from 'react-redux'
import { changeHeaderConfigAction } from '@/store/main'

const Detail = memo(() => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(changeHeaderConfigAction({isFixed: false, padding: 24, topAlpha: false}))
  }, [dispatch])

  return (
    <DetailWrapper>
      <AppHeader/>
      <DetailPicture/>
      <DetailInfos/>
    </DetailWrapper>
  )
})

export default Detail