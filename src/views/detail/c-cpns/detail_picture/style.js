import styled from "styled-components";

export const PictureWrapper = styled.div`
  position: relative;

  .pictures {
    display: flex;
    height: 600px;
    background-color: #000;

    &:hover {
      .cover {
        opacity: 1 !important;
      }
      .item:hover {
        .cover {
          opacity: 0 !important;
        }
      }
    }    
    .left, .right {
      width: 50%;
      height: 100%;

      .item {
        position: relative;
        height: 100%;
        overflow: hidden;
        box-sizing: border-box;
        border: 1px solid #232323;
        cursor: pointer;

        &:hover {
          img {
            transform: scale(1.08);
          }
        }
        img {
          width: 100%;
          height: 100%;
          object-fit: cover;
          transition: transform 500ms ease;
        }
        .cover {
          position: absolute;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          background-color: rgba(0,0,0,.2);
          opacity: 0;
        }
      }
    }
    .right {
      display: flex;
      flex-wrap: wrap;
      
      .item {
        width: 50%;
        height: 50%;
      }
    }
  }
  .show-btn {
    position: absolute;
    right: 24px;
    bottom: 24px;
    padding: 6px 15px;
    font-weight: 700;
    line-height: 22px;
    background-color: #fff;
    border-radius: 5px;
    cursor: pointer;
  }
`