import PropTypes from 'prop-types'
import React, { memo, useState } from 'react'
import { useSelector } from 'react-redux'

import { PictureWrapper } from './style'
import PictureBrowser from '@/base_ui/picture_browser'

const DetailPicture = memo((props) => {
  const { detailInfo } = useSelector((state) => ({
    detailInfo: state.detailReducer.detailInfo
  }))
  const [showBrowser, setShowBrowser] = useState(false)

  function closeBtnClick() {
    setShowBrowser(false)
  }

  return (
    <PictureWrapper>
      <div className='pictures'>
        <div className='left'>
          <div className='item' onClick={() => setShowBrowser(true)}>
            <img src={detailInfo?.picture_urls?.[0]} alt="" />
            <div className='cover'></div>
          </div>
        </div>
        <div className='right'>
          {
            detailInfo?.picture_urls?.slice(1, 5).map(item => {
              return (
                <div 
                  className='item' 
                  key={item} 
                  onClick={() => setShowBrowser(true)}
                >
                  <img src={item} alt="" />
                  <div className='cover'></div>
                </div>
              )
            })
          }
        </div>
      </div>
      <div className='show-btn' onClick={() => setShowBrowser(true)}>查看照片</div>
      {
        showBrowser && 
        <PictureBrowser
          pictureUrls={detailInfo?.picture_urls}
          closeBtn={closeBtnClick}
        />
      }
    </PictureWrapper>
  )
})

DetailPicture.propTypes = {
  detailInfo: PropTypes.object
}

export default DetailPicture