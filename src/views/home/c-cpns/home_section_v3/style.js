import styled from "styled-components";

const SectionV3Wrapper = styled.div`
  .v3-info {
    margin: 0 -10px;
  }
`
export default SectionV3Wrapper