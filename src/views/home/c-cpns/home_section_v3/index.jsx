import ScrollView from '@/base_ui/scroll_view'
import RoomItem from '@/components/room_item'
import SectionFooter from '@/components/section_footer'
import SectionHeader from '@/components/section_header'
import PropTypes from 'prop-types'
import React, { memo } from 'react'
import SectionV3Wrapper from './style'

const HomeSectionV3 = memo((props) => {
  const { sectionData } = props

  return (
    <SectionV3Wrapper>
      <SectionHeader title={sectionData.title} subtitle={sectionData.subtitle}/>
      <div className='v3-info'>
        <ScrollView>
          {
            sectionData.list.map(item => {
              return (<RoomItem itemData={item} itemWidth="20%" key={item.id}/>)
            })
          }
        </ScrollView>
      </div>
      <SectionFooter name="plus"/>
    </SectionV3Wrapper>
  )
})

HomeSectionV3.propTypes = {
  sectionData: PropTypes.object
}

export default HomeSectionV3