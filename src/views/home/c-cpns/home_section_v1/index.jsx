import PropTypes from 'prop-types'
import React, { memo } from 'react'

import SectionV1Wrapper from './style'
import SectionHeader from '@/components/section_header'
import SectionRooms from '@/components/section_rooms'
import SectionFooter from '@/components/section_footer'

const HomeSectionV1 = memo((props) => {
  const { sectionData } = props

  return (
    <SectionV1Wrapper>
      <SectionHeader title={sectionData.title} subtitle={sectionData.subtitle}/>
      <SectionRooms roomList={sectionData.list} itemWidth="25%"/>
      <SectionFooter/>
    </SectionV1Wrapper>
  )
})

HomeSectionV1.propTypes = {
  sectionData: PropTypes.object
}

export default HomeSectionV1