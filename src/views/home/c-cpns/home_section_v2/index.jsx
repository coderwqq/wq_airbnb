import PropTypes from 'prop-types'
import React, { memo, useCallback, useState } from 'react'

import SectionV2Wrapper from './style'
import SectionHeader from '@/components/section_header'
import SectionTabs from '@/components/section_tabs'
import SectionRooms from '@/components/section_rooms'
import SectionFooter from '@/components/section_footer'

const HomeSectionV2 = memo((props) => {
  const { sectionData } = props

  const tabsName = sectionData?.dest_address?.map(item => item.name)

  const initialName = Object.keys(sectionData.dest_list)[0]
  const [ tabName, setTabName ] = useState(initialName)
  const tabsClickHandle = useCallback(function(name) {
    setTabName(name)
  }, [])

  return (
    <SectionV2Wrapper>
      <SectionHeader title={sectionData?.title} subtitle={sectionData?.subtitle}/>
      <SectionTabs tabsName={tabsName} tabsClick={tabsClickHandle}/>
      <SectionRooms roomList={sectionData.dest_list?.[tabName]} itemWidth="33.333%"/>
      <SectionFooter name={tabName}/>
    </SectionV2Wrapper>
  )
})

HomeSectionV2.propTypes = {
  sectionData: PropTypes.object
}

export default HomeSectionV2