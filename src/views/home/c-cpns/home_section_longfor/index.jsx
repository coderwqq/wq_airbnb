import ScrollView from '@/base_ui/scroll_view'
import LongforItem from '@/components/longfor_item'
import SectionHeader from '@/components/section_header'
import PropTypes from 'prop-types'
import React, { memo } from 'react'
import LongforWrapper from './style'

const HomeSectionLongfor = memo((props) => {
  const { sectionData } = props
  
  return (
    <LongforWrapper>
      <SectionHeader title={sectionData.title} subtitle={sectionData.subtitle}/>
      <div className='item-info'>
        <ScrollView>
          {
            sectionData.list.map(item => {
              return <LongforItem itemData={item} key={item.city}/>
            })
          }
        </ScrollView>
      </div>
    </LongforWrapper>
  )
})

HomeSectionLongfor.propTypes = {
  sectionData: PropTypes.object
}

export default HomeSectionLongfor