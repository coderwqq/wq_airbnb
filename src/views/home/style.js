import styled from "styled-components";

const HomeWrapper = styled.div`
  user-select: none;
  
  > .content {
    width: 1032px;
    margin: 32px auto 0;

    > div {
      margin-top: 30px;
    }
  }
`

export default HomeWrapper