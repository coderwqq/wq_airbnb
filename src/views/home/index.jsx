import React, { memo, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { fetchHomeDataAction } from '@/store/home'
import { changeHeaderConfigAction } from '@/store/main'
import HomeWrapper from './style'
import AppHeader from '@/components/app_header'
import HomeBanner from './c-cpns/home_banner'
import HomeSectionV1 from './c-cpns/home_section_v1'
import HomeSectionV2 from './c-cpns/home_section_v2'
import HomeSectionV3 from './c-cpns/home_section_v3'
import HomeSectionLongfor from './c-cpns/home_section_longfor'

const Home = memo(() => {
  const { discountData, goodPriceData, highScoreData, hotrecommendData, longforData, plusData } = useSelector((state) => ({
    discountData: state.homeReducer.discountData,
    goodPriceData: state.homeReducer.goodPriceData,
    highScoreData: state.homeReducer.highScoreData,
    hotrecommendData: state.homeReducer.hotrecommendData,
    longforData: state.homeReducer.longforData,
    plusData: state.homeReducer.plusData
  }))

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchHomeDataAction())
    dispatch(changeHeaderConfigAction({isFixed: true, padding: 24, topAlpha: true}))
  }, [dispatch])

  return (
    <HomeWrapper>
      <AppHeader/>
      <HomeBanner/>
      <div className='content'>
        {!!Object.keys(discountData).length && <HomeSectionV2 sectionData={discountData}/>}
        {!!Object.keys(hotrecommendData).length && <HomeSectionV2 sectionData={hotrecommendData}/>}
        {!!Object.keys(longforData).length && <HomeSectionLongfor sectionData={longforData}/>}
        {!!Object.keys(goodPriceData).length && <HomeSectionV1 sectionData={goodPriceData}/>}
        {!!Object.keys(highScoreData).length && <HomeSectionV1 sectionData={highScoreData}/>}
        {!!Object.keys(plusData).length && <HomeSectionV3 sectionData={plusData}/>}
      </div>
    </HomeWrapper>
  )
})

export default Home