import React, { memo, useState } from 'react'

import { DemoWrapper } from './style'
import Indicator from '@/base_ui/indicator'

const Demo = memo(() => {
  const names = ["aaa","bbb","ccc", "eee", "fff","hhh","kkk",]

  const [selectIndex, setSelectIndex] = useState(0)

  function btnClickHandle(isNext) {
    let newIndex = isNext ? selectIndex + 1 : selectIndex -1
    if(newIndex < 0) newIndex = names.length - 1
    if(newIndex > names.length -1) newIndex = 0
    setSelectIndex(newIndex)
  }

  return (
    <DemoWrapper>
      <button onClick={() => btnClickHandle(false)}>上一个</button>
      <button onClick={() => btnClickHandle(true)}>下一个</button>
      <div className='names'>
        <Indicator selectIndex={selectIndex}>
          {
            names.map(item => {
              return (
                <button key={item}>{item}</button>
              )
            })
          }
        </Indicator>
      </div>
    </DemoWrapper>
  )
})

export default Demo