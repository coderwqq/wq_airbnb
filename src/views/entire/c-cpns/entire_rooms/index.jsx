import PropTypes from 'prop-types'
import React, { memo } from 'react'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { RoomsWrapper } from './style'
import { changeDetailInfoAction } from '@/store/detail'
import RoomItem from '@/components/room_item'

const EntireRooms = memo((props) => {
  const { roomList, totalCount, isLoding } = useSelector((state) => ({
    roomList: state.entireReducer.roomList,
    totalCount: state.entireReducer.totalCount,
    isLoding: state.entireReducer.isLoding
  }), shallowEqual)

  const navigate = useNavigate()
  const dispatch = useDispatch()
  
  function roomClickHandle(itemData) {
    // window.scrollTo(0, 0)
    dispatch(changeDetailInfoAction(itemData))
    navigate("/detail")
  }

  return (
    <RoomsWrapper>
      <div className='entire-room-text'>{totalCount}多处住宿</div>
      <div className='entire-wrap'>
        <div className='entire-room-info'>
          {
            roomList.map(item => {
              return (
                <RoomItem 
                  itemData={item} 
                  itemWidth="20%" 
                  key={item._id}
                  roomClick={roomClickHandle}
                />
              )
            })
          }
        </div>
        {isLoding && <div className='room-cover'></div>}
      </div>
    </RoomsWrapper>
  )
})

EntireRooms.propTypes = {
  roomsInfo: PropTypes.array
}

export default EntireRooms