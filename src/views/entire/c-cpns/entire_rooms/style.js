import styled from "styled-components";

export const RoomsWrapper = styled.div`
  margin-top: 140px;
  padding: 30px 80px 40px;

  .entire-room-text {
    font-size: 22px;
    font-weight: 700;
    margin-bottom: 8px;
    color: #222;
  }
  .entire-wrap {
    position: relative;

    .room-cover {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: rgba(255, 255, 255, .6);
    }
    .entire-room-info {
      display: flex;
      flex-wrap: wrap;
      margin: 0 -10px;
    }
  }
`