import styled from "styled-components";

export const PaginationWrapper = styled.div`
  display: flex;
  justify-content: center;

  .pagination {
    display: flex;
    flex-direction: column;
    align-items: center;

    .MuiPaginationItem-page {
      margin-right: 10px;
    }
    .Mui-selected {
      color: #fff;
      background-color: #484848;
      
      &:hover {
        color: #fff;
        background-color: #484848;
      }
    }
    .text {
      margin-top: 18px;
      color: #222;
    }
  }
`