// import PropTypes from 'prop-types'
import React, { memo } from 'react'
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import Pagination from '@mui/material/Pagination';

import { PaginationWrapper } from './style'
import { fetchEntireListDataAction } from '@/store/entire/actionCreate';

const EntirePagination = memo((props) => {
  const { currentPage, totalCount } = useSelector((state) => ({
    currentPage: state.entireReducer.currentPage,
    totalCount: state.entireReducer.totalCount
  }), shallowEqual)

  const dispatch = useDispatch()
  function handlePageChange(event, newPage) {
    // 跳转到顶部
    window.scrollTo(0, 0)
    
    dispatch(fetchEntireListDataAction(newPage-1))
  }

  const pageCount = Math.ceil(totalCount/20)
  const startCount = currentPage * 20 + 1
  const endCount = (currentPage + 1) * 20

  return (
    <PaginationWrapper>
      <div className='pagination'>
        <Pagination count={pageCount} onChange={handlePageChange}/>
        <div className='text'>第{startCount}-{endCount}个房源, 共超过{totalCount}个</div>
      </div>
    </PaginationWrapper>
  )
})

EntirePagination.propTypes = {
  
}

export default EntirePagination