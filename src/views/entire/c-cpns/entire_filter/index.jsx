import React, { memo, useState } from 'react'

import { FilterWrapper } from './style'
import filterData from "@/assets/data/filter_data"
import classNames from 'classnames'

const EntireFilter = memo((props) => {
  const [selectItems, setSelectItems] = useState([])

  function itemClickHandle(item) {
    const newItems = [...selectItems]
    if(selectItems.includes(item)) {
      const index = selectItems.findIndex(existItem => existItem === item)
      newItems.splice(index, 1)
    } else {
      newItems.push(item)
    }
    setSelectItems(newItems)
  }

  return (
    <FilterWrapper>
      <div className='filter-item'>
        {
          filterData.map(item => {
            return (
              <div 
                className={classNames("item", {active: selectItems.includes(item)})}
                key={item}
                onClick={() => itemClickHandle(item)}
              >
                {item}
              </div>
            )
          })
        }
      </div>
    </FilterWrapper>
  )
})


export default EntireFilter