import styled from "styled-components";

export const FilterWrapper = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  top: 80px;
  z-index: 9;
  display: flex;
  align-items: center;
  height: 60px;
  background-color: #fff;
  border-top: 1px solid #eee;
  /* box-shadow: 0 2px 4px rgba(0,0,0,.05); */
  
  .filter-item {
    display: flex;
    align-items: center;
    padding-left: 72px;  

    .item {
      padding: 8px 12px;
      margin: 0 4px 0 8px;
      border: 1px solid ${props => props.theme.color.lineColor};
      border-radius: 3px;
      cursor: pointer;

      &.active {
        color: #fff;
        background-color: ${props => props.theme.color.secondaryColor};
      }
      &:hover {
        ${props => props.theme.mixin.boxShadow}
      }
    }
  }
`
