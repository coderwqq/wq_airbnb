import React, { memo, useEffect } from 'react'
import { useDispatch } from 'react-redux'

import { fetchEntireListDataAction } from '@/store/entire/actionCreate'
import EntireWrapper from './style'
import EntireFilter from './c-cpns/entire_filter'
import EntirePagination from './c-cpns/entire_pagination'
import EntireRooms from './c-cpns/entire_rooms'
import AppHeader from '@/components/app_header'
import { changeHeaderConfigAction } from '@/store/main'

const Entire = memo(() => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchEntireListDataAction())
    dispatch(changeHeaderConfigAction({isFixed: true, padding: 80, topAlpha: false}))
  }, [dispatch])

  return (
    <EntireWrapper>
      <AppHeader/>
      <EntireFilter/>
      <EntireRooms/>
      <EntirePagination/>
    </EntireWrapper>
  )
})

export default Entire