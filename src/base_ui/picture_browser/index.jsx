import PropTypes from 'prop-types'
import React, { memo, useEffect, useState } from 'react'
import classNames from 'classnames'
import { SwitchTransition, CSSTransition } from 'react-transition-group'

import { BrowserWrapper } from './style'
import IconClose from '@/assets/svg/icon_close'
import IconArrowLeft from '@/assets/svg/icon_arrow_left'
import IconArrowRight from '@/assets/svg/icon_arrow_right'
import Indicator from '../indicator'
import IconTrigonDown from '@/assets/svg/icon_trigon_down'
import IconTrigonUp from '@/assets/svg/icon_trigon_up'

const PictureBrowser = memo((props) => {
  const { pictureUrls, closeBtn } = props
  const [currentIndex, setCurrentIndex] = useState(0)
  const [isNext, setIsNext] = useState(true)
  const [showList, setShowList] = useState(true)

  useEffect(() => {
    document.body.style.overflow = "hidden"
    return () => {
      document.body.style.overflow = "auto"
    }
  })
  function closeBtnClick() {
    if(closeBtn) closeBtn()
  }
  function controlBtnClick(isRight = true) {
    let newIndex = isRight ? currentIndex + 1 : currentIndex - 1
    if(newIndex < 0) newIndex = pictureUrls.length - 1
    if(newIndex > pictureUrls.length - 1) newIndex = 0
    setCurrentIndex(newIndex)
    setIsNext(isRight)
  }

  function isShowClick(isShow) {
    setShowList(isShow)
  }
  function toggleClick(index) {
    setIsNext(index > currentIndex)
    setCurrentIndex(index)
  }

  return (
    <BrowserWrapper isNext={isNext} showList={showList}>
      <div className='top'>
        <div className='close-btn' onClick={closeBtnClick}>
          <IconClose/>
        </div>
      </div>
      <div className='center'>
        <div className='control'>
          <div className='left' onClick={() => controlBtnClick(false)}>
            <IconArrowLeft width="77" height="77"/>
          </div>
          <div className='right' onClick={() => controlBtnClick(true)}>
            <IconArrowRight width="77" height="77"/>
          </div>
        </div>
        <div className='picture'>
          <SwitchTransition mode='in-out'>
            <CSSTransition 
              key={pictureUrls?.[currentIndex]}
              classNames="pic"
              timeout={200}
            >
              <img src={pictureUrls?.[currentIndex]} alt="" />
            </CSSTransition>
          </SwitchTransition>
        </div>
      </div>
      <div className='bottom'>
        <div className='inner'>
          <div className='info'>
            <div className='left'>
              <span>{currentIndex + 1}/{pictureUrls.length}: </span>
              <span>room apartment图片{currentIndex + 1}</span>
            </div>
            <div className='right' onClick={() => isShowClick(!showList)}>
              <span>{showList ? "隐藏" : "显示"}照片列表</span>
              {showList ? <IconTrigonDown/> : <IconTrigonUp/>}
            </div>
          </div>
          <div className='list'>
            <Indicator selectIndex={currentIndex}>
              {
                pictureUrls.map((item, index) => {
                  return (
                    <div 
                      className={classNames('item', {active: currentIndex === index})}
                      onClick={() => toggleClick(index)}
                    >
                      <img src={item} alt="" />
                    </div>
                  )
                })
              }
            </Indicator>
          </div>
        </div>
      </div>
    </BrowserWrapper>
  )
})

PictureBrowser.propTypes = {
  pictureUrls: PropTypes.array
}

export default PictureBrowser