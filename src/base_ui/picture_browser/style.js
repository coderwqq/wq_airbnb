import styled from "styled-components";

export const BrowserWrapper = styled.div`
  position: fixed;
  z-index: 999;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background-color: #282828;
  display: flex;
  flex-direction: column;

  .top {
    position: relative;
    height: 86px;

    .close-btn {
      position: absolute;
      top: 18px;
      right: 28px;
      cursor: pointer;
    }
  }
  .center {
    position: relative;
    flex: 1;
    display: flex;
    justify-content: center;
    
    .control {
      position: absolute;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      display: flex;
      justify-content: space-between;
      align-items: center;
      color: #fff;

      .left, .right {
        cursor: pointer;
      }
    }
    .picture {
      position: relative;
      width: 100%;
      max-width: 105vh;
      height: 100%;
      overflow: hidden;

      img {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        margin: 0 auto;
        height: 100%;
        user-select: none;
      }
    }
    .pic-enter {
      transform: translateX(${props => props.isNext ? "100%" : "-100%"});
      opacity: 0;
    }
    .pic-enter-active {
      transform: translateX(0);
      opacity: 1;
      transition: all 200ms ease;
    }
    .pic-exit {
      opacity: 1;
    }
    .pic-exit-active {
      opacity: 0;
    }
  }
  .bottom {
    display: flex;
    justify-content: center;
    height: 100px;
    margin-bottom: 24px;
    color: #fff;
    user-select: none;

    .inner {
      position: relative;

      .info {
        display: flex;
        width: 105vh;
        margin-bottom: 12px;

        .right {
          position: absolute;
          right: 0;
          cursor: pointer;
        }
      }

      .list {
        width: 105vh;
        overflow: hidden;
        height: ${props => props.showList ? "67" : "0"}px;
        transition: height 300ms ease;

        .item {
          img {
            width: 100px;
            margin-right: 10px;
            opacity: 0.5;
            cursor: pointer;
          }
          &.active {
            img {
              opacity: 1;
            }
          }
        }
      }
    }
  } 
`