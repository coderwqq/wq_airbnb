import styled from "styled-components";

const ViewWrapper = styled.div`
  position: relative;
   
  .scroll {
    overflow: hidden;

    .slot-content {
      display: flex;
      transition: transform 250ms ease;
    }
  }
  .control {
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #fff;
    z-index: 9;
    width: 28px;
    height: 28px;
    border-radius: 50%;
    color: #000;
    border: 1px solid #ccc;
    box-shadow: 0 2px 4px rgba(0, 0, 0, .2);

    &.left {
      left: 0;
      top: 50%;
      transform: translate(-50%, -50%);
    }
    &.right {
      right: 0;
      top: 50%;
      transform: translate(50%, -50%);
    }
  }

`
export default ViewWrapper