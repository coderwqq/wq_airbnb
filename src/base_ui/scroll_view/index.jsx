// import PropTypes from 'prop-types'
import IconArrowLeft from '@/assets/svg/icon_arrow_left'
import IconArrowRight from '@/assets/svg/icon_arrow_right'
import React, { memo, useEffect, useRef, useState } from 'react'

import ViewWrapper from './style'

const ScrollView = memo((props) => {
  const [showRight, setShowRight] = useState(false)
  const [showLeft, setShowLeft] = useState(false)
  const [positionIndex, setPositionIndex] = useState(0)
  const totalDistanceRef = useRef()

  // 组件渲染完毕，判断是否显示右侧按钮
  const scrollContentRef = useRef()
  useEffect(() => {
    const scrollWidth = scrollContentRef.current.scrollWidth
    const clientWidth = scrollContentRef.current.clientWidth
    const totalScrollDistance = scrollWidth - clientWidth
    setShowRight(totalScrollDistance > 0)
    totalDistanceRef.current = totalScrollDistance
  }, [props.children])

  
  function controlBtnClick(isRight) {
    const newIndex = isRight ? positionIndex + 1 : positionIndex -1
    const newElement = scrollContentRef.current.children[newIndex]
    const newOffsetLeft = newElement.offsetLeft
    console.log(newOffsetLeft)
    scrollContentRef.current.style.transform = `translate(-${newOffsetLeft}px)`
    setPositionIndex(newIndex)

    setShowLeft(newOffsetLeft > 0)
    // 是否继续显示右侧按钮
    setShowRight(totalDistanceRef.current - newOffsetLeft > 0)
  }

  return (
    <ViewWrapper>
      {showLeft && (
        <div className='control left' onClick={() => controlBtnClick(false)}>
          <IconArrowLeft/>
        </div>
      )}
      {showRight && (
        <div className='control right' onClick={() => controlBtnClick(true)}>
          <IconArrowRight/>
        </div>
      )}
      <div className='scroll'>
        <div className='slot-content' ref={scrollContentRef}>
          {props.children}
        </div>
      </div>
    </ViewWrapper>
  )
})

ScrollView.propTypes = {}

export default ScrollView