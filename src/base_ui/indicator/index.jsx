import PropTypes from 'prop-types'
import React, { memo, useEffect, useRef } from 'react'

import { IndicatorWrapper } from './style'

const Indicator = memo((props) => {
  const { selectIndex = 0 } = props

  const contentRef = useRef()
  useEffect(() => {
    const selectElement = contentRef.current.children[selectIndex]
    const itemOffsetLeft = selectElement.offsetLeft
    const itemWidth = selectElement.clientWidth

    const contentWidth = contentRef.current.clientWidth
    const scrollWidth = contentRef.current.scrollWidth
    // 获取selectIndex要滚动的距离
    let distance = itemOffsetLeft + itemWidth * 0.5 - contentWidth * 0.5

    const allowWidth = scrollWidth - contentWidth 
    if(distance < 0) distance = 0 //左边特殊情况的判断
    if(distance > allowWidth) distance = allowWidth //右边特殊情况的判断

    contentRef.current.style.transform = `translate(${-distance}px)`
  }, [selectIndex])

  return (
    <IndicatorWrapper>
      <div className='wrapper' ref={contentRef}>
        {props.children}
      </div>
    </IndicatorWrapper>
  )
})

Indicator.propTypes = {
  selectIndex: PropTypes.number
}

export default Indicator